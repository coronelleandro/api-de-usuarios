﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UsuarioApi.RequestModel;

namespace UsuarioApi.Controllers
{
    public class UsuarioController : ApiController
    {
        private UsuarioEntities context = new UsuarioEntities();

        [HttpPost]
        public IHttpActionResult crearUsuario(UsuarioRequest usuario)
        {
            
            Usuario nuevoUsuario = new Usuario();
            nuevoUsuario.nombre = usuario.nombre;
            nuevoUsuario.nombreUsuario = usuario.nombreUsuario;
            nuevoUsuario.usuarioPassword = usuario.usuarioPassword;
            nuevoUsuario.email = usuario.email;
            context.Usuarios.Add(nuevoUsuario);
            context.SaveChanges();
            return Ok("exito");
        }
        
        [HttpGet]
        public IHttpActionResult usuarios()
        {
            List<Usuario>usuarios = context.Usuarios.ToList();
            return Ok(usuarios);
        }

        [HttpPost]
        public IHttpActionResult ingresar(UsuarioRequest usuario)
        {
            Usuario usuarioBuscado = context.Usuarios
                              .Where(u => usuario.usuarioPassword == u.usuarioPassword)
                              .Where(u => usuario.email == u.email)
                              .FirstOrDefault();

            return Ok(usuarioBuscado);
        }

        [HttpGet]
        public IHttpActionResult buscarUsuario(int id)
        {
            Usuario usuario = context.Usuarios.Find(id);
            return Ok(usuario);
        }

        [HttpDelete]
        public IHttpActionResult borrarUsuario(int id)
        {
            Usuario usuario = context.Usuarios.Find(id);
            context.Usuarios.Remove(usuario);
            return Ok("deleteado"); 
        }

        [HttpPut]
        public IHttpActionResult actualizarUsuario(Usuario usuario)
        {
            
            Usuario usuarioActualizado = context.Usuarios.Find(usuario.idUsuario);
            usuarioActualizado.nombre = usuario.nombre;
            usuarioActualizado.nombreUsuario = usuario.nombreUsuario;
            usuarioActualizado.usuarioPassword = usuario.usuarioPassword;

            context.Usuarios.Attach(usuarioActualizado);
            context.Entry(usuarioActualizado).State = EntityState.Modified;
            context.SaveChanges();

            return Ok("updateado");
        }

    }
}
