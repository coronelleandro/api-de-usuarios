﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UsuarioApi.RequestModel
{
    public class UsuarioRequest
    {
        public int idUsuario { set; get; }
        public String nombre { set; get; }
        public String nombreUsuario { set; get; }
        public String usuarioPassword { set; get; }
        public String email { set; get; }
    }
}